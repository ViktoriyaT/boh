

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class SevletPagina
 */
@WebServlet(name = "SevletPagin", description = "serpag", urlPatterns = { "/SevletPagin" })
public class SevletPagina extends HttpServlet {
	private final String user="demo";
	private final String password="demo";
	
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
		String username= req.getParameter("username");
		String password = req.getParameter("password");
		
		if(user.equals(username) && password.equals(password)) {
			
			
			HttpSession session=req.getSession(false);
			if(session!=null) {
				session.invalidate();
			}
			HttpSession Newsession=req.getSession(false);
			Newsession.setAttribute(user, username);
			Newsession.setMaxInactiveInterval(5*60); //5 min di inattività
			try {
				resp.sendRedirect("sucess.jsp");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
		else {
			try {
				resp.sendRedirect("Form.jsp");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
	}
	
	

}
