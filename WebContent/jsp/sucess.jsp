<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Ti sei loggato con successo</title>
</head>
<body>
<%
String user=null;
if(session.getAttribute("user")==null)
	response.sendRedirect("Logi.jsp");
else 
	user=(String)session.getAttribute("user");


%>
<h1>Ciao, ${user}</h1>

</body>
</html>